# Mythic Maps

## Fonctional

### Exploration mode

#### Player perspective
- Interactive world map viewer
  - Move: Move the map to see other regions
  - Zoom: Displays a more detailed map
  - Click: Zooms in on selected location and displays description
- Description panel
  - Side panel description of selection
- Quick navigation panel
  - To quickly select an item
- Note-taking panel
  - To write additional notes on an item
  - User-specific
- Map selection menu. Navigate and selecy maps from your portfolio.

#### Game master perspective
- Show/hide elements to players in real time

### Editor mode
- Accessible only to certain roles
- Add elements: 
  - Images
  - Descriptions (of places, for example)
  - Clickable areas
- Notes:
  - Minimum zoom level before displaying an object
  - Objects organized in hierarchies
  - Editing rights management

## Architecture
- Web application
- Hosted on a server
- Accessible from any device

- Technos:
	- Front-end: Angular
		- Canvas creation: PixiJS
	- Back: ?
	- Storage: ?
	- Containerization: Docker
	- Project management: Gitlab
	- Wiki: Gitlab