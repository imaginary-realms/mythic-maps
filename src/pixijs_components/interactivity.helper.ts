import * as PIXI from 'pixi.js';
import 'pixi.js/math-extras';

type InteractionType = ('click'|'move'|'zoom')[];

/**
 * Helper class used to add interactivity to PIXI.Container.
 * 
 * Example:
 * ```ts
 * let sprite = PIXI.Sprite.from('sample.png');
 * InteractivityHelper.addInteraction(sprite, ['move', 'zoom']);
 * ```
 */
export class InteractivityHelper {
    static addInteraction(element: PIXI.Container, interactions: InteractionType) {
        element.eventMode = 'static';
        if(interactions.includes('move')) {
            this.addMove(element);
        }
        if(interactions.includes('zoom')) {
            this.addZoom(element);
        }
        if(interactions.includes('click')) {
            // Not yet implemented
        }
    }

    private static addMove(element: PIXI.Container) {
        element.on('pointerdown', onDragStart);
        element.on('pointerup', onDragEnd);
        element.on('pointerupoutside', onDragEnd);
    }

    private static addZoom(element: PIXI.Container) {
        element.on('wheel', onWheelRolled);
    }
}

function onDragStart(event: PIXI.FederatedPointerEvent) {
    let element = event.currentTarget;
    element.on('pointermove', onDragMove);
}

function onDragMove(event: PIXI.FederatedPointerEvent) {
    let element = event.currentTarget;
    element.x += event.movementX;
    element.y += event.movementY;
}

function onDragEnd(event: PIXI.FederatedPointerEvent) {
    let element = event.currentTarget;
    element.off('pointermove');
}

function getOffsetFromTarget(event: PIXI.FederatedMouseEvent) {
    let targetPosition = event.currentTarget.position;
    let mousePositionRelToCanvas = event.client;
    return new PIXI.Point(
        mousePositionRelToCanvas.x - targetPosition.x, 
        mousePositionRelToCanvas.y - targetPosition.y);
}

function onWheelRolled(event: PIXI.FederatedWheelEvent) {
    let target = event.currentTarget;
    
    // Get the current mouse position relative to the target container before any zooming occurs.
    const mouseOffsetBeforeZoom = getOffsetFromTarget(event);
    
    // Determine the zoom factor based on the scroll input. A constant scrollSpeed is used to scale the delta Y value of the wheel event.
    // A negative value for scrollSpeed inverts the zoom direction.
    const scrollSpeed: number = -0.001;
    const zoomFactor = 1 + event.deltaY * scrollSpeed;
    // Apply the zoom to the target by scaling its size using the calculated zoom factor.
    target.scale = target.scale.multiplyScalar(zoomFactor);
    
    // Calculate the new position of the mouse relative to the target container after zooming.
    const newMouseOffset = mouseOffsetBeforeZoom.multiplyScalar(zoomFactor);
    // Determine how much the target should move to keep the zoom centered around the mouse position.
    const movementVector = newMouseOffset.subtract(mouseOffsetBeforeZoom);
    // Adjust the target's position by subtracting the calculated movement vector, effectively repositioning it
    // so that the zoom remains centered on the mouse location.
    target.position = target.position.subtract(movementVector);
}