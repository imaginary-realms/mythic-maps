import { bootstrapApplication } from '@angular/platform-browser';
import { appConfig } from './angular_components/app/app.config';
import { AppComponent } from './angular_components/app/app.component';

bootstrapApplication(AppComponent, appConfig)
  .catch((err) => console.error(err));
