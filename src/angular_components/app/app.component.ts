import {
  Component,
  ElementRef,
  inject,
  NgZone,
  OnInit,
  signal,
  viewChild,
} from '@angular/core';
import { RouterOutlet } from '@angular/router';
import * as PIXI from 'pixi.js';
import { InteractivityHelper } from '../../pixijs_components/interactivity.helper';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent implements OnInit {
  pixiContainer = viewChild.required<ElementRef>('pixiContainer');
  private app = new PIXI.Application();
  private ngZone = inject(NgZone);

  sidebarVisible = signal<boolean>(false);

  ngOnInit(): void {
    (globalThis as any).__PIXI_APP__ = this.app; // Enable PixiJS DevTools extension support

    this.ngZone.runOutsideAngular(async () => {
      await this.app.init({
        resizeTo: window,
        background: '#f7f4e6',
      });
      this.pixiContainer().nativeElement.appendChild(this.app.canvas);

      await PIXI.Assets.load('test_map.webp');
      await PIXI.Assets.load('sample.png');

      let sprite = PIXI.Sprite.from('test_map.webp');
      let spriteChild = PIXI.Sprite.from('sample.png');

      let containerChild = new PIXI.Container();
      containerChild.addChild(spriteChild);

      InteractivityHelper.addInteraction(this.app.stage, ['move', 'zoom']);

      this.app.stage.addChild(sprite);
      this.app.stage.addChild(containerChild);
    });
  }

  onToggleSidebar() {
    this.sidebarVisible.update((isVisible) => !isVisible);
  }

  onImageAdded(event: any) {
    const file: File = event.target.files[0];
    console.log('File: ' + file);
    console.log(file);
    const fileUrl: string = URL.createObjectURL(file);
    console.log('FileURL: ' + fileUrl);
    const loadedFilePromise = PIXI.Assets.load({src: fileUrl, loader: 'loadTextures'});
    loadedFilePromise.then((texture) => {
      console.log('Texture: ' + texture);
      const sprite = PIXI.Sprite.from(texture);
      console.log('Sprite: ' + sprite);
      this.app.stage.addChild(sprite);
    })
  }
}