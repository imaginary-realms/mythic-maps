# Mythic Maps

Welcome to **Mythic Maps**! Dive into a world where creating and exploring detailed, interactive maps for role-playing games becomes a reality. 

Whether you're guiding players through epic quests or crafting intricate story settings, Mythic Maps provides the tools you need to bring your adventures to life.

## Overview

Mythic Maps is a web-based application designed to enrich tabletop role-playing games. 

It offers an immersive experience for both Game Masters and players, providing a dynamic platform for map exploration and customization.

### Key Features

- **Interactive World Map Viewer**: A gateway to exploring mythical lands.
  - **Move & Zoom**: Navigate through expansive regions with ease.
  - **Click & Discover**: Unveil secrets and detailed descriptions of mystical places.
- **Description Panel**: A dedicated space for lore and legend.
- **Quick Navigation Panel**: Leap across vast distances to notable landmarks in an instant.
- **Note-Taking Panel**: Personalize your journey with notes and annotations.
- **Map Selection Menu**: Your portfolio of magical realms, all in one place.

### Perspectives

#### For Players: Exploration Mode

- **Immersive Viewing**: Engage with the world through an interactive map, discovering the unknown.
- **Quick Access & Annotations**: Easily select and annotate points of interest.

#### For Game Masters: Control & Creativity

- **Real-Time Customization**: Reveal or conceal elements, controlling the flow of the narrative.
- **Comprehensive Editor**: Add images, descriptions, and interactive elements to craft your story.

### Editor Mode: Unleash Your Imagination

- **Role-Specific Access**: Tailored editing capabilities for creators.
- **Advanced Customization**: Integrate detailed imagery, lore, and interactive hotspots.
- **Strategic Organization**: Manage content with hierarchical structuring and zoom-level sensitivity.

## Technical Blueprint

Mythic Maps stands on the pillars of modern web technology, ensuring a seamless and scalable experience.

- **Platform**: Robust web application, accessible across devices.
- **Hosting**: Cloud-based infrastructure for global accessibility.

### Technology Stack

- **Front-End**: Angular for a responsive UI, enhanced with PixiJS for dynamic canvas rendering.
- **Back-End**: A question mark, awaiting the perfect fit.
- **Storage & Data Management**: Secure and scalable solutions under consideration.
- **Containerization**: Docker for streamlined development and deployment.
- **Collaboration & Documentation**: GitLab serves as the central hub for project management and wiki-based documentation.

Thank you for choosing Mythic Maps. Let the adventure begin!

## How to run

Install: `npm install`

Run locally: `npm start`

---

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
